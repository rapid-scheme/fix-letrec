;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \procedure{(fix-letrec expr)}

;;> Performs the procedure described in "Fixing Letrec (reloaded)" by
;;> Abdulaziz Ghuloum and R. Kent Dybvig.  It takes a syntax object
;;> encapsulating an expression of the input language and returns a
;;> syntax object encapsulating an expression of the output language.

;;> The input language has the following expressions: \scheme{(begin
;;> ...)}, \scheme{(if ...)}, \scheme{(set! ...)}, \scheme{(quote
;;> ...)}, \scheme{(case-lambda ...)}, \scheme{(letrec*-values ...)},
;;> procedure calls, and variable references.

;;> The output language has the following expressions: \scheme{(begin
;;> ...)}, \scheme{(if ...)}, 
;;> \scheme{(set-values! ...)}, \scheme{(quote
;;> ...)}, \scheme{(case-lambda ...)}, \scheme{(letrec ...)},
;;> \scheme{(let-values ...)}, procedure calls, and variable references.

(define (fix-letrec expr)
  (receive (free-vars complex? expr assigned-vars)
      (%fix-letrec expr)
    expr))

(define (%fix-letrec expr)
  (syntax-match expr
    ((begin ,(free-vars* complex?* expr* assigned-vars*) ...)
     (values (set-union* free-vars*)
	     (or* complex?*)
	     (syntax (begin ,expr* ...))
	     (set-union* assigned-vars*)))
    ((if ,(test-free-vars test-complex? test-expr test-assigned-vars)
	 ,(consequent-free-vars consequent-complex? consequent-expr
				consequent-assigned-vars)
	 ,(alternate-free-vars alternate-complex? alternate-expr
			       alternate-assigned-vars))
     (values (set-union test-free-vars
			consequent-free-vars
			alternate-free-vars)
	     (any test-complex?
		  consequent-complex?
		  alternate-complex?)
	     (syntax (if ,test-expr ,consequent-expr ,alternate-expr))
	     (set-union test-assigned-vars
			consequent-assigned-vars
			alternate-assigned-vars)))
    ((set! ,var ,(free-vars complex? expr assigned-vars))
     (values (set-adjoin free-vars (unwrap-syntax var))
	     #t
	     (syntax (set-values! (,var) ,expr))
	     (set-adjoin assigned-vars (unwrap-syntax var))))
    ((quote ,literal)
     (values (set symbol-comparator)
	     #f
	     (syntax (quote ,literal))
	     (set symbol-comparator)))
    ((case-lambda ((,arg1** ... . ,arg2**)
		   ,(free-vars* complex?* body* assigned-vars*)) ...)
     (values (set-union* (map (lambda (free-vars arg1* arg2*)
				(set-delete-all* free-vars
						 (map unwrap-syntax arg1*)
						 (unwrap-syntax arg2*)))
			      free-vars* arg1** arg2**))
	     #f
	     (syntax (case-lambda ((,arg1** ... . ,arg2**) ,body*) ...))
	     (set-union* assigned-vars*)))    
    ((letrec*-values (((,arg1** ... . ,arg2**)
		       ,(init-free-vars* init-complex?* init-expr*
					 init-assigned-vars*))
		      ...)
		     ,(body-free-vars body-complex? body-expr body-assigned-vars))
     (let*
	 ((assigned-vars
	   (set-union (set-union* init-assigned-vars*) body-assigned-vars))
	  (bindings
	   (list->vector
	    (let loop ((arg1** arg1**)
		       (arg2** arg2**)
		       (free-vars* init-free-vars*)
		       (complex*? init-complex?*)
		       (expr* init-expr*)
		       (i 0))
	      (if (null? free-vars*)
		  '()
		  (cons
		   (make-binding (car free-vars*)
				 (car complex*?)
				 (car expr*)
				 (set* symbol-comparator
				       (map unwrap-syntax (car arg1**))
				       (unwrap-syntax (car arg2**)))
				 (append (car arg1**) (car arg2**)))
		   (loop (cdr arg1**)
			 (cdr arg2**)
			 (cdr free-vars*)
			 (cdr complex*?)
			 (cdr expr*)
			 (+ i 1)))))))
	  (binding-mapping
	   (let loop ((i 0))
	     (if (= i (vector-length bindings))
		 (mapping symbol-comparator)
		 (set-fold (lambda (arg mapping)
			     (mapping-set mapping arg i))
			   (loop (+ i 1))
			   (binding-formal-set (vector-ref bindings i))))))
	  (dep-graph
	   (let loop ((dep-graph (digraph (make-comparator integer? = < #f)))
		      (previous-complex #f)
		      (i 0))
	     (if (= i (vector-length bindings))
		 dep-graph
		 (let*-values
		     (((binding)
		       (vector-ref bindings i))
		      ((dep-graph)
		       (set-fold (lambda (free-var dep-graph)
				   (cond
				    ((mapping-ref/default binding-mapping
							  free-var
							  #f)
				     => (lambda (j)
					  (binding-reference! (vector-ref bindings j))
					  (digraph-adjoin-edge dep-graph
							       j
							       i)))
				    (else
				     dep-graph)))
				 dep-graph (binding-free-vars binding)))
		      ((dep-graph previous-complex)
		       (cond
			((binding-complex? binding)
			 (when previous-complex
			   (values (digraph-adjoin-edge dep-graph
							previous-complex
							i)
				   i))
			 (values dep-graph i))
			(else
			 (values dep-graph previous-complex)))))
		   (loop dep-graph previous-complex (+ i 1))))))
	  (sccs
	   (digraph-sccs dep-graph)))
       (set-for-each (lambda (var)
		       (and-let* ((j (mapping-ref/default binding-mapping
							  var
							  #f)))
			 (binding-reference! (vector-ref bindings j))))
		     body-free-vars)
       (receive (expr)
	   (fold-right
	    (lambda (scc expr)
	      (if (= 1 (length scc))
		  ;; Single group
		  (let ((binding (vector-ref bindings (car scc))))
		    (cond
		     ((lambda-binding? assigned-vars binding)
		      (if (binding-referenced? binding)
			  (syntax
			   (letrec ((,(binding-formals binding)
				     ,(binding-init binding)))
			     ,expr))
			  expr))
		     ((not (set-contains? (digraph-successor-set dep-graph (car scc))
					  (car scc)))
		      (cond
		       ((binding-referenced? binding)
			(syntax
			 (let-values ((,(binding-formals binding)
				       ,(binding-init binding)))
			   ,expr)))
		       ((binding-complex? binding)
			(syntax
			 (begin ,(binding-init binding)
				,expr)))
		       (else
			expr)))
		     (else
		      (make-definitions
		       binding 
		       (syntax
			(begin (set-values! ,(binding-formals binding)
					    ,(binding-init binding))
			       ,expr))))))		  
		  ;; Multiple group
		  (receive (sorted-bindings)
		      (sort-bindings assigned-vars bindings scc)
		    (mapping-fold
		     (lambda (index complex? expr)
		       (if complex?
			   (make-definitions (vector-ref bindings index)
					     expr)
			   expr))
		     (let ((pairs
			    (mapping-fold
			     (lambda (index complex? pairs)
			       (if complex?
				   pairs
				   (let ((binding (vector-ref bindings index)))
				     (syntax
				      ((,(binding-formals binding)
					,(binding-init binding))
				       . ,pairs)))))
			     '() sorted-bindings))
			   (body
			    (mapping-fold/reverse
			     (lambda (index complex? expr)
			       (if complex?
				   (let ((binding (vector-ref bindings index)))
				     (syntax
				      (begin (set-values! ,(binding-formals binding)
							  ,(binding-init binding))
					     ,expr)))
				   expr))
			     expr
			     sorted-bindings)))
		       (if (null? pairs)
			   body
			   (syntax (letrec ,pairs ,body))))
		     sorted-bindings))))
	    (syntax ,body-expr) sccs)
	 (values (set-union (set-union*
			     (map
			      (lambda (free-vars arg1* arg2*)
				(set-delete-all* free-vars
						 (map unwrap-syntax arg1*)
						 (unwrap-syntax arg2*)))
			      init-free-vars* arg1** arg2**))
			    body-free-vars)
		 (or (or* init-complex?*)
		     body-complex?)
		 expr
		 assigned-vars))))
    ((,(free-vars* complex?* expr* assigned-vars*) ...)
     (values (set-union* free-vars*)
	     #t
	     (syntax (,expr* ...))
	     (set-union* assigned-vars*)))
    (,expr
     (values (set symbol-comparator (unwrap-syntax expr))
	     #f
	     expr
	     (set symbol-comparator (unwrap-syntax expr))))))

;;; Letrec*-values bindings

(define (make-binding free-vars complex? init formal-set formals)
  (vector free-vars complex? init formal-set formals #f))
(define (binding-free-vars binding) (vector-ref binding 0))
(define (binding-complex? binding) (vector-ref binding 1))
(define (binding-init binding) (vector-ref binding 2))
(define (binding-formal-set binding) (vector-ref binding 3))
(define (binding-formals binding) (vector-ref binding 4))
(define (binding-referenced? binding) (vector-ref binding 5))
(define (binding-reference! binding) (vector-set! binding 5 #t))

(define (lambda-binding? assigned-vars binding)
  (let ((formals (binding-formals binding)))
    (and (pair? formals)
	 (null? (cdr formals))
	 (not (set-contains? assigned-vars (unwrap-syntax (car formals))))
	 (syntax-match (binding-init binding)
	   ((case-lambda . ,_) #t)
	   (,_ #f)))))

(define (sort-bindings assigned-vars bindings indices)
  (fold (lambda (index mapping)
	  (let ((binding (vector-ref bindings index)))
	    (mapping-set mapping index (not (lambda-binding? assigned-vars
							     binding)))))
	(mapping (make-comparator integer? = < #f))
	indices))

(define (make-definitions binding body)
  (set-fold
   (lambda (arg expr)
     (syntax
      (let-values (((,arg) (if #f #f)))
	,expr)))
   body
   (binding-formal-set binding)))

;;; Utility procedures

(define (set* comparator element1* element2*)
  (let ((set (list->set comparator element1*)))
    (if (null? element2*)
	set
	(set-adjoin set element2*))))

(define (set-union* set*)
  (apply set-union set*))

(define (set-delete-all* set element1* element2*)
  (let ((set (set-delete-all set element1*)))
    (if (null? element2*)
	set
	(set-delete set element2*))))

(define (or* obj*)
  (and (not (null? obj*))
       (or (car obj*)
	   (or* (cdr obj*)))))
