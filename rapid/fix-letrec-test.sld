;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid fix-letrec-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid syntax)
	  (rapid fix-letrec))
  (begin
    (define (run-tests)
      (test-begin "Fixing letrec")

      (test-group "Examples"
	(define expr1
	  (syntax (letrec*-values (((q) '8)
				   ((f) (case-lambda ((x) (+ x q))))
				   ((r) (f q))
				   ((s) (+ r (f '2)))
				   ((g) (case-lambda (() (+ r s))))
				   ((t) (g)))
				  t)))

	(define expr2
	  (syntax (letrec*-values (((x) (list (case-lambda (() y))))
				   ((f) (case-lambda (() (cons x y))))
				   ((y) (list (case-lambda (() x))))
				   ((t) (f)))
				  t)))

	(test-equal "Example 1"
	  '(let-values
	       (((q) '8))
	     (let-values (((f) (case-lambda ((x) (+ x q)))))
	       (let-values (((r) (f q)))
		 (let-values (((s) (+ r (f '2))))
		   (let-values (((g) (case-lambda (() (+ r s)))))
		     (let-values (((t) (g))) t))))))
	  (syntax->datum (fix-letrec expr1)))

	(test-equal "Example 2"
	  '(let-values (((y) (if #f #f)))
	     (let-values (((x) (if #f #f)))
	       (begin (set-values! (x) (list (case-lambda (() y))))
		      (begin (set-values! (y) (list (case-lambda (() x))))
			     (let-values (((f) (case-lambda (() (cons x y)))))
			       (let-values (((t) (f))) t))))))
	  (syntax->datum (fix-letrec expr2))))

      (test-end))))
